.. Console Logs documentation master file, created by
   sphinx-quickstart on Tue Dec 26 15:54:36 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

#############
Console Logs!
#############

.. toctree::
    :caption: Log Entries
    :glob:

    entries/2018/01/*

.. toctree::
    :caption: Projects
    :glob:

    projects/arm/*

